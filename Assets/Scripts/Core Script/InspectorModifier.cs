﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelController))]
public class InspectorModifier : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LevelController script = (LevelController)target;

        if(GUILayout.Button("Start Level"))
        {
            script.StartLevel();
        }
    }
}
