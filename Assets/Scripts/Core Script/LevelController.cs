﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class LevelController : MonoBehaviour
{
    [Header("General Setting")]
    public CinemachineVirtualCamera virtualCamera;
    public CinemachineSmoothPath path;
    private CinemachineTrackedDolly dolly;
    public float lerpSpeed = 100f;

    private bool startedLevel;
    private float currentPosition;

    private void Start() {
        dolly = virtualCamera.GetCinemachineComponent<CinemachineTrackedDolly>();
    }
    private void Update() 
    {
        if(startedLevel)
        {
            if(currentPosition < 1f)
            {
                currentPosition += lerpSpeed / path.PathLength * Time.deltaTime;
            }
            else if(currentPosition > 1f)
            {
                currentPosition = 1f;
            }

            if(dolly.m_PathPosition != currentPosition)
                dolly.m_PathPosition = currentPosition;
        }
    }

    public void StartLevel()
    {
        currentPosition = 0f;
        startedLevel = true;
    }

}
