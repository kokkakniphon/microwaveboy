﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    public Collider2D col;
    //public Collider2D colTrigger;
    public int floorNum;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        laneCheck();
    }

    void laneCheck()
    {
        if (PlayerLaneSwitch.floorNum == floorNum)
        {
            col.enabled = true;
        }
        else
        {
            col.enabled = false;
        }
    }
}
