﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public Rigidbody2D rb;
    public SpriteRenderer sr;
    public Animator anim;
    public float speed;
    private float speedCopy;
    public float speedDecrease;
    public float speedRecovery;
    public float speedRecoveryDelay;
    private bool isImmortal;
    public float jumpForce;
    public float additionalJumpDelayTime;
    private bool canJump;
    private bool jumpOneTime;
    private int jumpCount;
    public float jumpUpGravity;
    public float jumpDownGravity;
    public static bool isJump;
    public float rayDistance;
    public float raycastoffset;
    public int srBlinkCountMax;
    private int srBlinkCount;
    public float srBlinkDelay;
    public static bool isSlide;
    private float slideDelay;
    public int additionalJump;
    public float slideStartStateDelay;
    public float slideEndStateDelay;

    // Start is called before the first frame update
    void Start()
    {
        speedCopy = speed;
        canJump = true;
        jumpOneTime = false;
        jumpCount = 0;
        isImmortal = false;
        isJump = false;
        isSlide = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (canJump && Input.GetKeyDown(KeyCode.Space))
        {
            jump();
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            slide();
        }
        if(Input.GetKeyUp(KeyCode.LeftControl))
        {
            returnSlide();
        }
        gravityControl();
        RaycastCheck();
    }

    void FixedUpdate()
    {
        if (jumpOneTime)
        {
            jumpOneTime = false;
            jumpPhysx();
        }
        rb.velocity = new Vector2(speed * Time.deltaTime, rb.velocity.y);
    }

    void jump()
    {
        if (isSlide)
        {
            isSlide = false;
            animationSetting(0, 1);
        }
        canJump = false;
        isJump = true;
        jumpOneTime = true;
        jumpCount++;
        //StartCoroutine(additionalJumpDelay());
        animationSetting(1, 1);
    }

    void jumpPhysx()
    {
        rb.velocity = Vector2.zero;
        rb.AddForce(new Vector2(0f, jumpForce * Time.deltaTime), ForceMode2D.Force);
    }

    void gravityControl()
    {
        if (!PlayerLaneSwitch.isChangeLane)
        {
            if (rb.velocity.y > 0)
            {
                rb.gravityScale = jumpUpGravity;
            }
            else
            {
                rb.gravityScale = jumpDownGravity;
                if (isJump && rb.velocity.y < 0)
                {
                    animationSetting(1, 2);
                }
            }
        }
        else
        {
            rb.gravityScale = 0f;
        }

        /*if (rb.velocity.y > 0)
        {
            rb.gravityScale = jumpUpGravity;
        }
        else
        {
            rb.gravityScale = jumpDownGravity;
        }*/
    }

    void RaycastCheck()
    {
        RaycastHit2D rc = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - raycastoffset), new Vector2(0, -1f), rayDistance);
        if (rc && isJump && rb.velocity.y < 0)
        {
            jumpCount = 0;
            canJump = true;
            isJump = false;
            animationSetting(0, 1);
        }
    }

    void srBlink()
    {
        srBlinkCount = 0;
        StartCoroutine(srDisappear(srBlinkDelay));
        if(srBlinkCount == srBlinkCountMax)
        {
            isImmortal = false;
        }
    }

    void slide()
    {
        if (!isJump)
        {
            if (!isSlide)
            {
                animationSetting(0, 1);
                animationSetting(2, 1);
                StartCoroutine(slideStartStateAnimDelay());
            }
            isSlide = true;
            //make microwave slide
        }
        else
        {
            isSlide = false;
        }
    }

    void returnSlide()
    {
        if (isSlide)
        {
            animationSetting(2, 3);
            StartCoroutine(slideEndStateAnimDelay());
        }
        isSlide = false;
        //make microwave not slide
    }

    void animationSetting(int command, int state)
    {
        if(command == 1)
        {
            anim.SetBool("isJump", true);
            anim.SetInteger("jumpState", state);
        }
        else if (command == 2)
        {
            anim.SetBool("isSlide", true);
            anim.SetInteger("slideState", state);
        }
        else
        {
            anim.SetBool("isJump", false);
            anim.SetInteger("jumpState", state);
            anim.SetBool("isSlide", false);
            anim.SetInteger("slideState", state);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "obstacle" && !isImmortal)
        {
            isImmortal = true;
            speed -= speedDecrease;
            srBlink();
            StartCoroutine(speedRecoveryOverTime());
        }
    }

    IEnumerator speedRecoveryOverTime()
    {
        yield return new WaitForSeconds(speedRecoveryDelay);
        speed += speedRecovery;
        if(speed < speedCopy)
        {
            StartCoroutine(speedRecoveryOverTime());
        }
        else if (speed > speedCopy)
        {
            speed = speedCopy;
        }
    }

    IEnumerator srDisappear(float delay)
    {
        yield return new WaitForSeconds(delay);
        srBlinkCount++;
        sr.enabled = false;
        if(srBlinkCount < srBlinkCountMax)
        {
            StartCoroutine(srAppear(delay));
        }
    }

    IEnumerator srAppear(float delay)
    {
        yield return new WaitForSeconds(delay);
        srBlinkCount++;
        sr.enabled = true;
        if (srBlinkCount < srBlinkCountMax)
        {
            StartCoroutine(srDisappear(delay));
        }
    }


    IEnumerator additionalJumpDelay()
    {
        yield return new WaitForSeconds(additionalJumpDelayTime);
        //Debug.Log("jumpcount : " + jumpCount + " and additionalJump " + additionalJump);
        if(jumpCount < additionalJump)
        {
            canJump = true;
        }
    }

    IEnumerator slideStartStateAnimDelay()
    {
        yield return new WaitForSeconds(slideStartStateDelay);
        animationSetting(2, 2);
    }

    IEnumerator slideEndStateAnimDelay()
    {
        yield return new WaitForSeconds(slideEndStateDelay);
        animationSetting(0, 1);
    }
}
