﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLaneSwitch : MonoBehaviour
{
    public static bool isChangeLane;
    public Rigidbody2D rb;
    public Collider2D col;
    public static int floorNum;
    private int floorNumTemp;
    public int startingLane;
    public int laneAmount;
    private bool canChange;
    public float translateUpAmount;
    public float translateDownAmount;

    private int changeLaneDirection;
    public float changeLaneTimeLength;

    public List<Transform> playerLanePos;

    private float velY;
    

    void Start()
    {
        col.enabled = true;
        isChangeLane = false;
        floorNum = startingLane;
        canChange = true;
        floorNumTemp = floorNum;
        //changeLaneDirection = 0;
    }
    void Update()
    {
        if (canChange)
        {
            if(Input.GetKeyDown(KeyCode.UpArrow) && floorNum > 1)
            {
                startChangeLane();
                changeLaneDirection = -1;
                floorNumTemp--;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow) && floorNum < laneAmount)
            {
                startChangeLane();
                changeLaneDirection = 1;
                floorNumTemp++;
            }
        }

        if (isChangeLane)
        {
            if (changeLaneDirection == -1)
            {
                transform.localPosition = new Vector3(Mathf.Lerp(transform.position.x, playerLanePos[floorNumTemp - 1].position.x, Time.deltaTime * translateUpAmount), Mathf.Lerp(transform.position.y, playerLanePos[floorNumTemp - 1].position.y, Time.deltaTime * translateUpAmount), transform.position.z);
            }
            else if (changeLaneDirection == 1)
            {
                transform.localPosition = new Vector3(Mathf.Lerp(transform.position.x, playerLanePos[floorNumTemp - 1].position.x, Time.deltaTime * translateDownAmount), Mathf.Lerp(transform.position.y, playerLanePos[floorNumTemp - 1].position.y, Time.deltaTime * translateDownAmount), transform.position.z);
            }
        }
    }

    void startChangeLane()
    {
        velY = rb.velocity.y;
        rb.velocity = new Vector2(rb.velocity.x, 0f);
        isChangeLane = true;
        canChange = false;
        col.enabled = false;
        StartCoroutine(endChangeLane());
    }

    IEnumerator endChangeLane()
    {
        yield return new WaitForSeconds(changeLaneTimeLength);
        changeLaneDirection = 0;
        isChangeLane = false;
        canChange = true;
        floorNum = floorNumTemp;
        col.enabled = true;
        rb.velocity = new Vector2(rb.velocity.x, velY);
    }


    /*void Update()
    {
        if (canChange)
        {
            if(Input.GetKeyDown(KeyCode.UpArrow) && floorNum > 1)
            {
                startChangeLane();
                changeLaneDirection = -1;
                floorNum--;
                StartCoroutine(endChangeLane());
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow) && floorNum < laneAmount)
            {
                startChangeLane();
                changeLaneDirection = 1;
                floorNum++;
                StartCoroutine(endChangeLane());
            }
        }

        if(changeLaneDirection != 0)
        {
            if(changeLaneDirection == -1)
            {
                goUp();
            }
            else if (changeLaneDirection == 1)
            {
                goDown();
            }
        }
    }

    void startChangeLane()
    {
        col.enabled = false;
        canChange = false;
        isChangeLane = true;
    }

    void goUp()
    {
        transform.Translate(Vector2.up * translateUpAmount * Time.deltaTime);
    }

    void goDown()
    {
        transform.Translate(Vector2.down * translateDownAmount * Time.deltaTime);
    }

    IEnumerator endChangeLane()
    {
        yield return new WaitForSeconds(changeLaneTimeLength);
        col.enabled = true;
        canChange = true;
        isChangeLane = false;
        changeLaneDirection = 0;
    }*/
}
