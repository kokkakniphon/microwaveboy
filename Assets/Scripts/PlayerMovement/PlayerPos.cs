﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPos : MonoBehaviour
{
    public Transform playerPos;
    public List<Transform> playerLanePos;
    float[] playerLaneStartingPosY;
    public int floorNum;
    public float posXOffset;
    //public List<float> playerLaneStartingPosY;

    // Start is called before the first frame update
    void Start()
    {
        playerLaneStartingPosY = new float[playerLanePos.Count];
        for(int i = 0; i< playerLanePos.Count; i++)
        {
            playerLaneStartingPosY[i] = playerLanePos[i].position.y;
        }
    }

    // Update is called once per frame
    void Update()
    {
        copyPlayerPos();
    }

    void copyPlayerPos()
    {
        transform.localPosition = new Vector3(playerPos.position.x + posXOffset, findPosY(), playerPos.position.z);
    }

    float findPosY()
    {
        float posY;
        if (!PlayerLaneSwitch.isChangeLane)
        {
            posY = playerLaneStartingPosY[floorNum - 1] + (playerPos.position.y - playerLaneStartingPosY[PlayerLaneSwitch.floorNum - 1]);
        }
        else
        {
            posY = transform.position.y;
        }
        //posY = playerLaneStartingPosY[floorNum - 1];
        return posY;
    }
}
